# Investimentos CNPq

EM CONSTRUÇÃO

Visualização do investimento do CNPq de 2002 a 2020 nas diferentes áreas de pesquisa em cada estado brasileiro. 

### Páginas com dados:
[Ciências Exatas e da Terra](https://raquissa.gitlab.io/investimentos-cnpq/exatas.html)

[Ciências Humanas](https://raquissa.gitlab.io/investimentos-cnpq/humanas.html)
